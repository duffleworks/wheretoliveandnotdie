var request = require('request');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

module.exports = function(url,method,data,credentials,callback) {
  request({
      url: url,
      method: method,
      encoding: null,
      headers: {
        'Connection': 'close',
      },
      rejectUnhauthorized: false,
      body: data,
    },
    function(error,response,body){
      if (error) {
        console.log(new Date() + ' ' + error);
        callback(error,null);
      }
      else{
        callback(null,body);
      }
  });
}
